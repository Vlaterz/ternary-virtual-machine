TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
CXX_FLAGS += -std=c++11

SOURCES += main.cpp \
    tritLib.cpp \
    cpu.cpp

HEADERS += \
    tritLib.h \
    cpu.h

