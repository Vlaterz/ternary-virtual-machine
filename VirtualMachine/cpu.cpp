#include "cpu.h"

CPU::CPU()
{

}

CPU::CPU(int Size)
{
    cout<<"creating cpu"<<endl;
    size=Size;
    memory = new Trit[Size];
    Trint temp;
    temp = 0;
    registers = { {"ax",temp},
                  {"bx",temp},
                  {"cx",temp},
                  {"max",temp},
                  {"mbx",temp},
                  {"mcx",temp}};
    run();
}

/*
 CPU::CPU(Trit *memo)
{
 cout<<"creating cpu"<<endl;
 size=1287; //??
 memory=memo;
 Trint temp;
 temp = 0;
 registers = { {"ax",temp},
               {"bx",temp},
               {"cx",temp},
               {"max",temp},
               {"mbx",temp},
               {"mcx",temp}};
 run();
}
*/
double CPU::transferToNum(string a) //кусок кода из предыдущих лаб , переводит строку в число
{
    if(isNum(a))
    {
    bool coma=0;
    for(unsigned int i=0 ; i<a.size() ; i++)
    {
        if(a[i]==44)
        {
            coma=1;
        }
    }
    if(coma==1)
    {

        double answer=0;
        double a1=0;
        double a2=0;
        string temp;
        int x=0;//запомнить место запятой
        int y=0;//запомнить кол во чисел после запятой
        for(unsigned int i=0;i<a.size();i++)
        {
            if(a[i]!=44)
            {
                temp.push_back(a[i]);
            }
            else
            {
                x=i;
                a1=transferToNum(temp);
            }
        }
        temp.clear();


        for(unsigned int i=x+1 ; i<a.size() ; i++)
        {
            y++;
            temp.push_back(a[i]);
        }

        a2=transferToNum(temp)/(pow(10,y));
        if(a1<0)
        {
            a1=-a1;
        }
        answer=a1+a2;
        if(a[0]==45)
        {
            answer=-answer;
        }
        return answer;

    }
    else
    {
        double answer=0;
            for(unsigned int i=0;i<a.size();i++)
            {
                if(a[i]==45)
                {
                    continue;
                }
                answer+=(static_cast<int>(a[i])-48)*pow(10,a.size()-i-1);
            }
            if(a[0]==45)
            {
                answer=-answer;
            }
            return answer;

    }


    }
}

string CPU::transferToString(double a) //кусок кода из предыдущих лаб , переводит число в строку
{
    int var1=0;
    float var2=a;
    int i=0;//число знаков после запятой

if( (int)a!=a )
{
    for( ; var1!=var2  ;i++ )
 {
         var2=var2*10;
         var1=var2;
 }
}
if(i==0)
{
    var1=(int)a;
    string answer;
    int k=0;
    int k1=0;
    int c=0;
    if(var1==0) //если число 0
    {
        answer.push_back(48);
        return answer;
    }
    if(var1<0)
    {
        var1=-var1;
        for(int i=10 ; k1 != var1 ; i*=10 ) //пихаем в строку значения (но задом-наперёд)
        {
            k=var1 % i;
            c=k;
            k=(k-k1)/(i/10);
            k1=c;
            answer.push_back(48+k);
        }
        answer.push_back(45);
    }
    else
    {
        for(int i=10 ; k1 != var1 ; i*=10 ) //пихаем в строку значения (но задом-наперёд)
        {
            k=var1%i;
            c=k;
            k=(k-k1)/(i/10);
            k1=c;
            answer.push_back(48+k);
        }

    }

    for(unsigned int i=0 ; i<answer.size()/2 ; i++) //переворачиваем строку
    {
        c=answer[i];
        answer[i]=answer[answer.size()-1-i];
        answer[answer.size()-1-i]=c;
    }
    //cout<<"transfer exit: "<<answer<<endl;
    return answer;
}
else
{
    bool minus=0; //отрицательное ли число
    string answer;
    var2=a;
    if(var2<0)
    {
        var2=-var2;
        minus=1;
    }
    var1=(int)var2;
    var2=var2-var1; //var2 = то,что после запятой 0,2123 0,0031
   // cout<<"var 2 = "<<var2<<endl;
    if(minus==1)
    {
        answer.push_back('-');
    }
    answer+=transferToString(var1);
    answer.push_back(',');
    for(int k=0 ; k<i ; k++)
    {
        var2=var2*10;
        if(var2<1)
        {
            answer.push_back('0');
        }
    }
   // cout<<"var 2 * 10^i = "<<var2<<endl;
    answer+=transferToString((int)var2);
    return answer;
}
}

void CPU::showMemorySegment(int seg)
{
    cout<<"showing memo start"<<endl;
    if(seg >(size / 27))
    {
    cout<<"ERROR: OUT OF RANGE"<<endl;
            return;
    }

    int tryteSeg = 27;
    int current=0;
    for(int i = 0 ; i<6 ; i++)
    {
        for(int j = current + seg*tryteSeg ; j<(current+seg*tryteSeg)+27 ; j++)
        {
            cout<<memory[j].val<<" ";
        }
        current+=27;
        cout<<endl;
    }
}

void CPU::showVariables()
{
    //list<tuple<string,string,int>> variables;//таблица переменных
    cout<<"VARIABLES: "<<endl;
    if(variables.size()==0)
    {
        cout<<"None"<<endl<<endl;
        return;
    }
    for(list<tuple<string,string,int>>::iterator it1 = variables.begin() ; it1!=variables.end();it1++)
    {
           cout<<get<0>(*it1)<<" "<<get<1>(*it1)<<" "<<get<2>(*it1)<<" ";//<<endl;
           if(get<0>(*it1) == "tfloat")
           {
               double value;
               Trint temp;
               temp.read(&memory[get<2>(*it1)]);
               value = (int)temp;
               temp.read(&memory[get<2>(*it1) + 27]);
               value = value / pow(10,(int)temp);
               cout<<value<<endl;
           }
           else if(get<0>(*it1) == "tchar")
           {
               char temp;
               Tchar temp1;
               temp1.data.read(&memory[get<2>(*it1)]);
               temp = (char)temp1;
               cout<<temp<<endl;
           }
           else if(get<0>(*it1) == "trint")
           {
               Trint temp;
               temp.read(&memory[get<2>(*it1)]);
               cout<<temp<<endl;
           }
    }

}

void CPU::showRegisters()
{
    cout<<"REGISTERS:"<<endl;
    for(map<string,Trint>::iterator it = registers.begin() ; it!=registers.end() ;it++)
    {
        cout<<it->first<<" : "<<it->second<<endl;
    }
    cout<<endl;
}



void CPU::write(Tryte data, int cell)
{
    //cout<<"write func"<<endl;

 for(int i = cell , j=0 ; i<27 ; i++,j++)
 {
     memory[i]=data.val[j];
 }
 showMemorySegment((int)(cell/27));
}

Tryte CPU::read(int cell)
{

}

vector<string> CPU::lexer(string str)
{

    vector<string> lexems;
    string temp;
    for(unsigned int i=0 ; i<str.size() ; i++)
    {
        if(str[i]!=' ')
        {
            temp.push_back(str[i]);
        }
        else
        {
            lexems.push_back(temp);
            temp.clear();
        }
    }
    return lexems;
}
void CPU::run()
{
    cout<<"making cycle start"<<endl;
    string command;
    for( ; ; )
    {
       cout<<"running cycle start"<<endl;
       getline(cin,command);
       command.push_back(' ');
       cout<<"Command : "<<command<<endl;
       vector<string> lexems = lexer(command);
       switch (parcer(lexems)) //обработчик ошибок
       {
       case 1:
           cout<<"Error"<<endl;
           break;
       case 2:
           cout<<"ERROR : syntax error"<<endl;
           break;
       case 3:
           cout<<"ERROR : variable already exists"<<endl;
       case 4:
           cout<<"ERROR : no such variable found"<<endl;
       }

    }
}
int CPU::parcer(vector<string>lexems)
{
    system("cls");
    /*
    stack<string> op;//операции
    stack<int> opNum;//расположение операции в строке
    for(unsigned int i=0 ; i<lexems.size() ; i++)//пушим в стек операции и их номер в списке
    {
        if(     lexems[i]=="+"||
                lexems[i]=="-"||
                lexems[i]=="="||
                lexems[i]=="*"||
                lexems[i]=="/")
        {
            op.push(lexems[i]);
            opNum.push(i);
        }
    }

    for( ; !op.empty() ; )
    {
       //обход строчки - заменяем все переменные на их значения!

       string operation = op.top();
       int operationNum = opNum.top();
       op.pop();
       opNum.pop();
       if(operation == "+")
       {
          // transferToNum()
           //lexems[operationNum];
       }
    }
    */

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if(isType(*lexems.begin())) //если тип данных
    {
        cout<<"Type"<<endl;
        if(!isNum(lexems[1]))// и если второе - слово, то значит переменная
        {
            cout<<"type and name : adding new variable..."<<endl;
            for(list<tuple<string,string,int>>::iterator it = variables.begin() ; it!=variables.end();it++)
            {
                if(lexems[1] == get<1>(*it))//если переменная уже существует
                {
                   return 2;
                }
            }
            int adr=size/2;
            for(list<tuple<string,string,int>>::iterator it = variables.begin() ; it!=variables.end();it++)//кладем ее в следующую незанятую ячейку
            {
                if(adr == get<2>(*it))
                {
                    if(get<0>(*it)=="tfloat")//для tfloat нужно 2 трайта
                    {
                        adr+=54;
                    }
                    else //в будущем ,если будут другие типы , можно измеять выделяемый размер.
                    {
                        adr+=27;//стандартный размер для тринта,тфлоата
                    }
                    it = variables.begin();
                }
            }
            tuple<string,string,int>var(lexems[0],lexems[1],adr);
            variables.push_back(var);
        }
    }
        //-----------------------------------------------------------------------------------------------------------------
        if(*lexems.begin()=="show") //команда "вывести на экран"
        {
            if(lexems.size() == 2)
            {
                if(isNum(lexems.at(1)))
                {
                showMemorySegment(transferToNum(lexems.at(1)));
                }
                else if(lexems.at(1)=="variables")
                {
                    showVariables();
                }
                else if(lexems.at(1) == "registers")
                {
                    showRegisters();
                }
            }
            else if(lexems.size() == 3)
            {
                if(isNum(lexems.at(1)) && isNum(lexems.at(2))) //вывод от одной ячейки до другой
                {
                    for(int i=transferToNum(lexems.at(1)) ; i<transferToNum(lexems.at(2)) ; i++)
                    {
                        cout<<memory[i].val<<" ";
                        if(i%26 == 0 && i!=transferToNum(lexems.at(1)))
                        {
                            cout<<endl;
                        }
                    }
                    cout<<endl;
                }
            }

        }
        if(*lexems.begin()=="mov") //команда "поместить"
        {
            if(isNum(lexems.at(1)))
            {
                return 1; //ошибка , не можем положить что-либо в число.
            }
            else if( varExists(lexems.at(1)))
            {
                //кладем в переменную
            }
            else if(registers.count(lexems.at(1)))//если в регистр
            {
                if(isNum(lexems.at(2))) //если число кладем
                {
                    if(isInt(transferToNum(lexems.at(2))))//наше число интовое?
                    {
                        Trint temp;
                        temp = transferToNum( lexems.at(2));
                        registers.at(lexems.at(1))=temp;
                    }
                    else //если дробное
                    {
                        Tfloat temp;
                        temp = transferToNum( lexems.at(2));
                        registers.at(lexems.at(1))=temp.data;
                        if(lexems.at(1)[0]!='m')//если название регистра начинается не с m
                        registers.at(string("m")+lexems.at(1))=temp.mantiss;//делаем мантиссу
                    }
                }
                else if(registers.count(lexems.at(2)))//если с другого регистра
                {
                    registers.at(lexems.at(1)) = registers.at(lexems.at(2));
                }
                else if(varExists(lexems.at(2)))//значение перменной
                {
                   //берем значение из переменной?
                }
            }
        }
        if(*lexems.begin()=="read" && lexems.size()==3)
        {
            if(registers.count(lexems.at(1)))//в регистр
            {
               if(isNum(lexems.at(2)))//по адресу
               {
                  int place = transferToNum(lexems.at(2));

                  if(place>size)
                  return 1;//вышли за пределы

                  registers.at(lexems.at(1)).read(&memory[place]);
               }
               else if(varExists(lexems.at(2)))//из переменной
               {
                    for(list<tuple<string,string,int>>::iterator it = variables.begin() ; it!=variables.end();it++)
                    {
                       if(get<1>(*it) == lexems.at(2))
                       {

                         if(get<0>(*it)=="tfloat") //tfloat присваивается несколько иначе (заполняются еще max mbx mcx)
                         {
                             registers.at(lexems.at(1)).read(&memory[get<2>(*it)]);
                             registers.at("m"+lexems.at(1)).read(&memory[get<2>(*it)+27]);
                         }
                         else //другие типы
                         {
                             registers.at(lexems.at(1)).read(&memory[get<2>(*it)]);
                         }
                       }
                    }
               }

            }
            else
            {
                return 1;//ошибка , считать можно только в регистр
            }
        }
        if(*lexems.begin() == "write" && lexems.size()==3)
        {
           if(isNum(lexems.at(1)))//записать по адресу
           {
               int place = transferToNum(lexems.at(1));
               if(isNum(lexems.at(2)))// из другого адреса? 1 ячейку?
               {
                   //нужно ли это? возможно...
               }
               else if(varExists(lexems.at(2)))//значение переменной
               {
                   for(list<tuple<string,string,int>>::iterator it = variables.begin() ; it!=variables.end();it++)
                   {
                      if(get<1>(*it) == lexems.at(2)) //нашли такую переменную
                      {
                         unsigned int place = transferToNum(lexems.at(1));
                         Tryte data ;
                         data.read(&memory[get<2>(*it)]);
                         write(data,place);
                      }
                   }
               }
               else if(registers.count(lexems.at(2)))//значение регистра . так можно записывать число по адресу
               {
                   registers.at(lexems.at(2)).write(&memory[place]);
               }
           }
           else if(varExists(lexems.at(1)))//в переменную
           {
               if(isNum(lexems.at(2))) // записываем число
               {
                   for(list<tuple<string,string,int>>::iterator it = variables.begin() ; it!=variables.end();it++)
                   {
                      if(get<1>(*it) == lexems.at(1))//нашли переменную
                      {

                          unsigned int place = get<2>(*it);

                          if(get<0>(*it)=="tfloat")
                          {
                              Tfloat temp;
                              temp=transferToNum(lexems.at(2));
                              cout<<temp<<" "<<place<<endl;
                              temp.write(&memory[place]);

                          }
                          else
                          {
                              Trint data;
                              data=(int)transferToNum(lexems.at(2));
                              cout<<data<<" "<<place<<endl;
                              data.write(&memory[place]);
                          }

                      }
                   }
               }
               else if(varExists(lexems.at(2))) //значение переменной
               {
                   for(list<tuple<string,string,int>>::iterator it = variables.begin() ; it!=variables.end();it++)
                   {
                      if(get<1>(*it) == lexems.at(1))//нашли 1 переменную
                      {
                          for(list<tuple<string,string,int>>::iterator it1 = variables.begin() ; it1!=variables.end();it1++)
                          {
                             if(get<1>(*it1) == lexems.at(2))//нашли 2 переменную
                             {
                                 Tryte data;
                                 if(get<0>(*it) == "tfloat" && get<0>(*it1) == "tfloat")//если обе еременные типа float
                                 {
                                     Tryte mantiss;
                                     data.read(&memory[get<2>(*it1)]);
                                     data.write(&memory[get<2>(*it)]);
                                     mantiss.read(&memory[get<2>(*it1) + 27]);
                                     mantiss.write(&memory[get<2>(*it) + 27]);
                                 }
                                 else
                                 {
                                   data.read(&memory[get<2>(*it1)]);
                                   data.write(&memory[get<2>(*it)]);
                                 }
                                 break;
                             }
                          }
                          break;
                      }
                   }
               }
               else if(registers.count(lexems.at(2)))//значение из регистра
               {
                   if(lexems.at(2).at(0) == 'm' ) //мы не можем использовать мантиссные регистры
                   {
                       return 1;
                   }
                   for(list<tuple<string,string,int>>::iterator it = variables.begin() ; it!=variables.end();it++)
                   {
                      if(get<1>(*it) == lexems.at(1))//нашли переменную
                      {
                          unsigned int place = get<2>(*it);

                          if(get<0>(*it)=="tfloat") //запись в флоат-переменную
                          {
                              Tfloat temp;
                              temp.data=registers.at(lexems.at(2)).data;
                              temp.mantiss=registers.at("m"+lexems.at(2)).data;
                              cout<<temp<<" "<<place;
                              temp.write(&memory[place]);

                          }
                          else
                          {
                              Trint data;
                              data.data=registers.at(lexems.at(2)).data;
                              cout<<data<<" "<<place<<endl;
                              data.write(&memory[place]);
                          }
                      }
                   }
               }

           }
        }
         if(*lexems.begin() == "add" && lexems.size()==3) //сложение двух регистров
         {
             if(registers.count(lexems.at(1)) && registers.count(lexems.at(2)))//если оба - регистры
             {
                 registers.at(lexems.at(1)) =  registers.at(lexems.at(1)) + registers.at(lexems.at(2));
             }
         }
         if(*lexems.begin() == "sub" && lexems.size()==3) //сложение двух регистров
         {
             if(registers.count(lexems.at(1)) && registers.count(lexems.at(2)))//если оба - регистры
             {
                 registers.at(lexems.at(1)) =  registers.at(lexems.at(1)) - registers.at(lexems.at(2));
             }
         }
         if(*lexems.begin() == "mul" && lexems.size()==3)
         {
             if(registers.count(lexems.at(1)) && registers.count(lexems.at(2)))//если оба - регистры
             {
                 registers.at(lexems.at(1)) =  registers.at(lexems.at(1)) * registers.at(lexems.at(2));
             }
         }
         if(*lexems.begin() == "div" && lexems.size()==3)
         {
             if(registers.count(lexems.at(1)) && registers.count(lexems.at(2)))//если оба - регистры
             {
                 registers.at(lexems.at(1)) =  registers.at(lexems.at(1)) / registers.at(lexems.at(2));
             }
         }
         if(*lexems.begin() == "div%" && lexems.size()==3)
         {
             if(registers.count(lexems.at(1)) && registers.count(lexems.at(2)))//если оба - регистры
             {
                 registers.at(lexems.at(1)) =  registers.at(lexems.at(1)) % registers.at(lexems.at(2));
             }
         }
         if(*lexems.begin() == "swap" && lexems.size()==3)
         {
             if(registers.count(lexems.at(1)) && registers.count(lexems.at(2)))//если оба - регистры
             {
                 Trint temp;
                 temp = registers.at(lexems.at(1));
                 registers.at(lexems.at(1)) = registers.at(lexems.at(2));
                 registers.at(lexems.at(2)) = temp;

             }
         }
         if(*lexems.begin() == "command")
         {
             /*
              write 0 mov
              write 27 ax
              write 54 18
             */
             if(registers.count(lexems.at(1)) && registers.count(lexems.at(2)))//если оба - регистры
             {
                 registers.at(lexems.at(1)) =  registers.at(lexems.at(1)) % registers.at(lexems.at(2));
             }
         }

    showRegisters();
    showVariables();
    return 0;
}
bool CPU::isType(string str)
{
    if(str == "trint" || str == "tchar" || str=="tfloat")
        return 1;
    else
        return 0;
}

bool CPU::varExists(string name) //существует ли такая переменная
{
            for(list<tuple<string,string,int>>::iterator it=variables.begin() ; it!=variables.end();it++)
            {
                if(get<1>(*it)==name)
                {
                    return 1;
                   // cout<<"var "<<name<<" EXISTS!"<<endl;
                }
            }
            return 0;
}

bool CPU::isNum(string a)
{
        bool flag = 1;
        unsigned int i=0;
        if(a[0]==45)//игнорируем минус
        {
            i++;
        }
        for( ; i<a.size() ; i++)
        {
           // cout<<a[i]<<" "<<(int)a[i]<<endl;
            if(((int)a[i]<48 || (int)a[i]>57) && (int)a[i]!=44 ) //если не цифра ,и не запятая  то уж точно не число
            {
             flag=0;
             break;
            }
            else
            {
                flag=1;
            }
        }
        return flag;
}

bool CPU::isCommand(string a)
{

}

bool CPU::isInt(double a)
{
 if(a==(int)a)
     return 1;
 else
     return 0;
}

