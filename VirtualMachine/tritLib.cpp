#include "tritLib.h"
#include "math.h"

int translate (tritCode code)//перевод триткода в число
{
    int value=0;
 for(int i=code.code.size()-1,j=0; i>=0 ; i--,j++)
 {
     value += code.code[i].val * (pow(3,j));//
 }
 return value;
}

int translateInt(tritCode code)
{
    int value=0;
 for(int i=code.code.size()-1,j=0; i>=1 ; i--,j++)
 {
     value += code.code[i].val * (pow(3,j));//
 }
    return value;
}

tritCode translate(int number)
{
  //  cout<<"TRANSLATING TO TRITCODE"<<endl;
  //  cout<<"NUMBER="<<number<<endl;
    bool minus=0;
    if(number<0)
    {
        minus=1;
    }
    number=abs(number);
    vector<int> tr;
    int i=4;
    int left=number;//left - осталось
    //int temp;
    tritCode trits;
    trits.clear();
   // cout<<"left="<<left<<endl;
    while(i >= 3)
    {
       tr.insert(tr.begin(),1,left%3);
       left=left/3;
       //cout<<left
       if(left<3 && left>=0)
       {
           tr.insert(tr.begin(),1,left);
           break;
       }
       else
       {
           i=left;
       }
    }
   // cout<<"trsize= "<<tr.size()<<endl;
    for(int i = tr.size()-1 ; i>=0 ; i--)//переводим в сбалансированную
    {
        if(tr[i]==2)
        {
            tr[i]=-1;
            if(i==0)//если двойка в начале , пушим 1 спереди
            {
                tr.insert(tr.begin(),1,1);
            }
            else
            {
                tr[i-1]++;
            }
        }
        if(tr[i]==3)
        {
            tr[i]=0;
            if(i==0)//если тройка в начале , пушим 1 спереди
            {
                tr.insert(tr.begin(),1,1);
            }
            else
            {
                tr[i-1]++;
            }
        }
    }
    if(minus)//переводим в отрицательное если число было со знаком -
    {
        for(unsigned int i = 0 ; i<tr.size() ; i++)
        {
            tr[i]=tr[i]*-1;
        }

        /*for (auto &t: tr)
            t *= -1;*/
    }
    for(unsigned int i = 0 ; i<tr.size() ; i++)//заполняем наш триткод
    {
        switch(tr[i])
        {
        case -1: trits.pushBack(Trit(-1));break;
        case 0: trits.pushBack(Trit(0));break;
        case 1: trits.pushBack(Trit(1));break;
        }
    }

    if(trits.isEmpty())
    {
        cout<<"returning empty"<<endl;
        return tritCode();
    }
    else
    {
        return trits;
    }

}

tritCode translateInt(int number)
{
    tritCode trits=translate(number);
    while(trits.size() < 27)
    {
        trits.pushForward(Trit(0));
    }
    return trits;
}

   tritCode::tritCode()
   {
       code.push_back(Trit(0));
   }


   /*tritCode::tritCode(Trit trits[])
   {

   }
*/
   tritCode::tritCode(Trit* trit , int size)
   {
      for(int i=0 ; i<size ; i++)
      {
          code.push_back(*trit);
          trit++;
      }
   }

   tritCode::tritCode(Tryte dat)
   {
       code.clear();
       for(int i = 0 ; i<27 ; i++)
       {
           code.push_back(dat.val[i]);
       }
   }

   tritCode::tritCode(int size)
   {
       code.resize(size);
   }

   void tritCode::pushBack(Trit trit)
   {
       code.push_back(trit);
   }

   void tritCode::pushForward(Trit trit)
   {
       code.insert(code.begin(),1,trit);
   }

   bool tritCode::isEmpty()
   {
       if (code.size()==0)
       {
           return 1;
       }
       else
       {
           return 0;
       }
   }

   void tritCode::clear()
   {
       code.clear();
   }

   void tritCode::show()
   {
       for(unsigned int i = 0 ; i<code.size() ; i++)
       {
           cout<<code[i].val<<" ";
       }
       cout<<endl;
   }

   void tritCode::update()
   {
       for(unsigned int i =0;i<code.size();i++)
       {
           if(code[i].val == 0)
           {
               code.erase(code.begin());
               i--;
           }
           else
           {
               return;
           }
       }
   }

   int tritCode::size()
   {
       return code.size();
   }

   void tritCode::operator =(tritCode a)
   {
      code=a.code;
   }
//-------------------------------------------------------------------------
Trit::Trit()
{
    val = Idk;
}

Trit::Trit(int a)
{
    switch(a)
    {
    case -1: val=False;break;
    case 0: val=Idk;break;
    case 1: val=True;break;
    }
    if(a>1 || a<-1)
    {
        val=Idk;
    }
}

void Trit::operator =(int a)
{
    //cout<<"operator = : "<<a<<endl;
    switch(a)
    {
    case -1: val=False;break;
    case 0: val=Idk;break;
    case 1: val=True;break;
    }
    cout<<val<<endl;
}
//--------------------------------------------------------------
Tryte::Tryte(Trit trits[27])
{
    cout<<"creating tryte "<<endl;
    size=27;
    for(int i=0 ; i<size ; i++)
    {
        val[i]=trits[i];
        //cout<<trits[i].val<<" ";
    }
}

Tryte::Tryte(tritCode code)
{
    //code.update();
    if(code.code.size() > 28)
    {
        cout<<"Tryte can`t contain tritCode , bigger than 27"<<endl;
        return;
    }
    for(int i=0 ; i<27 ; i++)
    {
        val[i]=Trit(0);
    }
    for(int i = 26,j=code.code.size()-1 ; j>=0;j--,i--)
    {
      val[i]=code.code[j];
    }
}
Tryte::Tryte()
{

}

void Tryte::operator =( Trit mass[27])
{
    for(int i=0 ; i<27 ; i++)
    {
        val[i]=mass[i];
    }

}
void Tryte::read(Trit* adress)
{
  for(int i = 0 ; i<27 ; i++)
  {
      val[i]=*adress;
      adress++;
  }
}

void Tryte::write(Trit *adress)
{
    for(int i = 0 ; i<27 ; i++)
    {
        adress[i]=val[i];
    }
}

void Tryte::show()
{
    for(int i=0 ; i<27 ; i++)
    {
        cout<<val[i].val<<" ";
    }
    cout<<endl;
}
//-----------------------------------------------------------------------
Tchar::Tchar(tritCode dat)
{
    data=Tryte(dat);
}

Tchar::Tchar(Tryte dat)
{
    data=dat;
    data.show();
}
Tchar::Tchar()
{

}
Tchar:: operator char() //перегрузка приведения из 3 в 10 сис счисл
{
    return (char)translate(tritCode(data));
}
/*ostream& Tchar:: operator << (ostream &s)
{
    s<<char(translate(tritCode(data)));  
}
*/
void Tchar::operator =(char symbol)
{
    data=Tryte(translate((int)(symbol)));
}

void Tchar::operator =(Tchar tchar)
{
  data=tchar.data;
}
//--------------------------------------------------------------------------

Trint::Trint(Tryte dat)
{
    data=dat;
}

Trint::Trint(tritCode dat)
{
    for(int i = 0 ; i<27;i++)
    {
      data.val[i]=Trit(0);
    }
    int j = dat.code.size()-1;
    for(int i=26 ; j>=0 && i>=0 ; j--,i--)
    {
        data.val[i] = dat.code[j];
    }
}

Trint::Trint()
{

}

void Trint::read(Trit *mem)
{
    tritCode alpha;
    alpha.clear();
    for(int i = 0 ;i < 27 ; i++)
    {
        alpha.pushBack(mem[i]);
    }
    data=Tryte(alpha);
}

void Trint::write(Trit *mem)
{
    for(int i = 0 ; i<27 ; i++)
    {
        mem[i]=data.val[i];
    }
}

Trint::operator int()
{
    return translateInt(tritCode(data));
}

/*ostream &Trint::operator <<(ostream &s)
{
    s<<translateInt(tritCode(data));
}
*/
void Trint::operator =(int num)
{
    data=Tryte(translateInt(num));
}

void Trint::operator =(Trint trint)
{
    data=trint.data;
}

const Trint operator-(const Trint &left, const Trint &right)
{
  tritCode exit;
  vector<int> trits;
  for(int i=0 ; i<27 ; i++)
  {
      trits.push_back(left.data.val[i].val + right.data.val[i].val*-1);
  }

  for(int i=trits.size()-1 ; i>=0 ; i--) //убираем 2 и -2
   {
      if(trits[i]==2)
      {
          if(i==0)
          {
              cout<<"int is too big"<<endl;
              return Trint();
          }
          trits[i]=-1;
          trits[i-1]++;
      }
      if(trits[i]==-2)
      {
          if(i==0)
          {
              cout<<"int is too big"<<endl;
              return Trint();
          }
          trits[i]=1;
          trits[i-1]--;
      }
      if(trits[i]==3)
      {
          if(i==0)
          {
              cout<<"int is too big"<<endl;
              return Trint();
          }
          trits[i]=0;
          trits[i-1]++;
      }
      if(trits[i]==-3)
      {
          if(i==0)
          {
              cout<<"int is too big"<<endl;
              return Trint();
          }
          trits[i]=0;
          trits[i-1]--;
      }
  }
      exit.clear();//чтобы не было лишнего трита
      for(unsigned int i = 0 ; i<trits.size() ; i++)//пушим в выходной триткод триты
       {
          switch(trits[i])
          {
          case -1: exit.pushBack(Trit(-1));break;
          case 0: exit.pushBack(Trit(0));break;
          case 1: exit.pushBack(Trit(1));break;
          }
       }
//exit.show();
      return Trint(Tryte(exit));


}
const Tfloat operator-(const Tfloat &left, const Tfloat &right)
{
   Tfloat ret;
   Tryte a;
   ret=right;
   vector<int> trits;
   for(int i=0 ; i<27 ; i++)
   {

   }
   for(int i=0 ; i<27 ; i++)
   {
       //cout<<ret.data.val[i].val<<" ";
       ret.data.val[i]=Trit(ret.data.val[i].val * -1);
   }
   Tfloat ans;
   ans = left+ret;
   return ans;
}

const Tfloat operator+(const Tfloat &left, const Tfloat &right)//22.1+ 122.12 = 144.22 .... 221 + 12212 = 22100+122120 = 144220 (144220 / 1000 = 144.22)
{                                                              //12.124+14.1=26.264 ....12124(0)+141(000) = 26к...
    Tfloat retStatement;
    //cout<<"OPERATOR + in FLOAT"<<endl;
    //cout<<"left="<<left<<" right="<<right<<endl;
     int leftI = translateInt(left.data); //нет умножения и деления , поэтому пока так... все в интах,а потом складываем в тритах
     int rightI = translateInt(right.data);
     int leftM = translateInt(left.mantiss);
     int rightM = translateInt(right.mantiss);
    //cout<<"leftI= "<<leftI<<" "<<"rightI="<<rightI<<endl;
    //cout<<"leftM= "<<leftM<<" "<<"rightM="<<rightM<<endl;

    if(abs(leftI)<abs(rightI))
    {
        leftI*=pow(10,abs(leftM-rightM) );
    }
    else
    {
         rightI*=pow(10,abs(leftM-rightM) );
    }
    //cout<<"leftI*= "<<leftI<<" "<<"rightI*="<<rightI<<endl;
    Trint LTrint; //создаем два тринта для сложения триткодов
    Trint RTrint;
    LTrint=leftI;
    RTrint=rightI;
    Trint ans;
    ans=LTrint+RTrint;

    //cout<<LTrint<<" "<<RTrint<<endl;
    //cout<<ans<<endl;

    retStatement.mantiss=translateInt(max(translateInt(left.mantiss),translateInt(right.mantiss)));

    retStatement.data=ans.data;
    return retStatement;
}
const Trint operator+(const Trint &left, const Trint &right)
{
  //cout<<"Trint op+:"<<endl;
  //cout<<left<<" "<<right<<endl;
  tritCode exit;
  vector<int> trits;
  trits.clear();
  for(int i = 1 ; i<27 ; i++)//складываем триты
   {
     int sum=left.data.val[i].val + right.data.val[i].val;
     trits.push_back(sum);
   }
  for(int i=trits.size()-1 ; i>=0 ; i--) //убираем 2 и -2, 3 и -3
   {
      if(trits[i]==2)
      {
          if(i==0)
          {
              cout<<"int is too big"<<endl;
              return Trint();
          }
          trits[i]=-1;
          trits[i-1]++;
      }
      if(trits[i]==-2)
      {
          if(i==0)
          {
              cout<<"int is too big"<<endl;
              return Trint();
          }
          trits[i]=1;
          trits[i-1]--;
      }
      if(trits[i]==3)
      {
          if(i==0)
          {
              cout<<"int is too big"<<endl;
              return Trint();
          }
          trits[i]=0;
          trits[i-1]++;
      }
      if(trits[i]==-3)
      {
          if(i==0)
          {
              cout<<"int is too big"<<endl;
              return Trint();
          }
          trits[i]=0;
          trits[i-1]--;
      }
   }
  for(unsigned int i = 0 ; i<trits.size() ; i++)//пушим в выходной триткод триты
   {
      switch(trits[i])
      {
      case -1: exit.pushBack(Trit(-1));break;
      case 0: exit.pushBack(Trit(0));break;
      case 1: exit.pushBack(Trit(1));break;
      }
   }
return Trint(Tryte(exit));
}

const tritCode operator*( tritCode &left,  tritCode &right) //WIP? 1221 1021001
{
    tritCode exit;
    vector<int> trits;
    vector<tritCode>toSum;
    tritCode temp;
    toSum.clear();
    temp.clear();
    trits.clear();

       for(int j=0; j<right.size() ; j++)//на что умножаем
       {
           for(int x=0 ; x<left.size() ; x++)//что умножаем
           {
               temp.pushBack(Trit(right.code[j].val*left.code[x].val ));
           }
           for(int k = 0 ; k<right.size()-j-1 ; k++)
           {
               temp.pushBack(Trit(0));
           }
           for(int k = 0  ; k <= j-1 ; k++)
           {
               temp.pushForward(Trit(0));
           }
           toSum.push_back(temp);
           temp.clear();
       }

for( int j = 0 ; j<toSum[0].size() ; j++)
{
    int temp=0;
    for(unsigned int i = 0 ; i<toSum.size() ; i++)
    {
        temp+=toSum[i].code[j].val;
    }
    trits.push_back(temp);
}

for(int i=trits.size()-1 ; i>=0 ; i--)
{
        if( abs(trits[i]) > 2 && trits[i]>0 )
        {
            int temp=trits[i]/3;
            trits[i]=trits[i]%3;
            trits[i-1]=trits[i-1]+temp;
        }
        else if( abs(trits[i]) > 2 && trits[i]<0)
        {
            int temp=trits[i]/3;
            trits[i]=trits[i]%3;
            trits[i-1]=trits[i-1]-temp;
        }
}

for(int i=trits.size()-1 ; i>=0 ; i--) //убираем 2 и -2, 3 и -3
 {
    if(trits[i]==2)
    {

        trits[i]=-1;
        if(i==0)
        {
        trits.insert(trits.begin(),1,1);
        }
        else
        {
        trits[i-1]++;
        }
    }
    if(trits[i]==-2)
    {

        trits[i]=1;
        if(i==0)
        {
        trits.insert(trits.begin(),1,-1);
        }
        else
        {
        trits[i-1]--;
        }
    }
    if(trits[i]==3)
    {
        trits[i]=0;
        if(i==0)
        {
            trits.insert(trits.begin(),1,1);
        }
        else
        {
        trits[i-1]++;
        }
    }
    if(trits[i]==-3)
    {
        trits[i]=0;
        if(i==0)
        {
             trits.insert(trits.begin(),1,-1);
        }
        else
        {
        trits[i-1]--;
        }
    }
 }

for(unsigned int i = 0 ; i<trits.size() ; i++)
{
    exit.pushBack(Trit(trits[i]));
}
exit.update();

return exit;
}
const Trint operator*(const Trint &left, const Trint &right)
{
    tritCode lefT(left.data);
    tritCode righT(right.data);
    return Trint(lefT*righT);
}

const Tfloat operator*(const Tfloat &left, const Tfloat &right)
{
   Tfloat exit;
   tritCode lefT(left.data);
   tritCode righT(right.data);


   tritCode exp;

   exp = lefT * righT;
   exp.update();

   exit.data = Tryte(exp);

   Trint lMant(left.mantiss);
   Trint rMant(right.mantiss);
   Trint exitMant;
   exitMant = lMant+rMant;
   exit.mantiss =exitMant.data;
   return exit;
}
const tritCode operator/(tritCode &left, tritCode &right)
{
    Trint Left(left);
    Trint Right(right);
    int ex=0;

    if((int)Right == 0)
    {
        cout<<" diving on zero is forbidden"<<endl;
        return left;
    }
    if((int)Left ==0)
    {
        return tritCode(translateInt(0));
    }
    for( ;abs((int)Left)>=abs((int)Right);  )
    {
        //cout<<(int)Left<<" "<<(int)Right<<endl;
        if( ((int)Left>=0 && (int)Right>=0)||((int)Left<=0 && (int)Right<=0))
        {
            Left = Left - Right;
            ex++;
        }
        else
        {
            Left = Left + Right;
            ex--;
        }
        //system("pause");
    }
    return tritCode(translateInt(ex));
}

const Trint operator/(const Trint &left, const Trint &right)
{
    tritCode ltc(left.data);
    tritCode rtc(right.data);
    return Trint(Tryte(ltc/rtc));
}


const Tfloat operator/(const Tfloat &left, const Tfloat &right)
{
    tritCode ltc(left.data);
    tritCode rtc(right.data);

    tritCode lm(left.mantiss);
    tritCode rm(right.mantiss);

    Trint one(lm);//забыл написать отдельно для триткода сложение вычитание , поэтому такие костыли
    Trint two(rm);
    two = one - two ;//НЕ БУДЕТ РАБОТАТЬ НОРМАЛЬнО ПОКА НЕТ ОСТАТКА ОТ ДЕЛЕНиЯ

    if(abs((int)Trint(ltc)) > abs((int)Trint(rtc)))
    {
        cout<<"/"<<endl;
        Tryte TR(ltc/rtc) ;
        Tryte MANT(two.data) ;//-
        Tfloat exit;
        exit.data=TR;
        exit.mantiss=MANT;
        return exit;
    }
    else
    {
        cout<<"%"<<endl;
        Tryte TR(ltc%rtc) ;
        Tryte MANT(two.data) ;//-
        Tfloat exit;
        exit.data=TR;
        exit.mantiss=MANT;
        return exit;
    }

}
const tritCode operator%(tritCode &left, tritCode &right)
{
    Trint Left(left);
    Trint Right(right);
    //int ex=0;

    if((int)Right == 0)
    {
        cout<<" diving on zero is forbidden"<<endl;
        return left;
    }
    if((int)Left ==0)
    {
        return tritCode(translateInt(0));
    }
    for( ;abs((int)Left)>=abs((int)Right);  )
    {
        if( ((int)Left>=0 && (int)Right>=0)||((int)Left<=0 && (int)Right<=0))
        {
            Left = Left - Right;
        }
        else
        {
            Left = Left + Right;
        }
    }
    return tritCode(Left.data);
}
const Trint operator%(const Trint &left, const Trint &right)
{
    tritCode ltc(left.data);
    tritCode rtc(right.data);
    return Trint(Tryte(ltc%rtc));
}
const Tfloat operator%(const Tfloat &left, const Tfloat &right)
{
    tritCode ltc(left.data);
    tritCode rtc(right.data);

    tritCode lm(left.mantiss);
    tritCode rm(right.mantiss);

    Trint one(lm);//забыл написать отдельно для триткода сложение вычитание , поэтому такие костыли
    Trint two(rm);
    two = one - two ;//НЕ БУДЕТ РАБОТАТЬ НОРМАЛЬнО ПОКА НЕТ ОСТАТКА ОТ ДЕЛЕНиЯ

    Tryte TR(ltc/rtc) ;
    Tryte MANT(two.data) ;//-
    Tfloat exit;
    exit.data=TR;
    exit.mantiss=MANT;
    return exit;
}
ostream &operator <<(ostream &s, Trint output)
{
    s<<translateInt(tritCode(output.data));
    return s;
}

ostream &operator <<(ostream &s, Tchar output)
{
    s<<char(translate(tritCode(output.data)));
    return s;
}
ostream& operator << (ostream &s , Tfloat output)
{
   // cout<<"tfloat << start"<<endl;
    s<<float(output);
   // cout<<"tfloat << end"<<endl;
    return s;
}

Tfloat::Tfloat()
{

}

void Tfloat::read(Trit *mem)
{
    tritCode alpha;
    tritCode beta;
    beta.clear();
    alpha.clear();
    for(int i = 0 ;i < 27 ; i++)
    {
        alpha.pushBack(mem[i]);
        beta.pushBack(mem[i+27]);
    }
    data=Tryte(alpha);
    mantiss=Tryte(beta);
}

void Tfloat::write(Trit *mem)
{
    for(int i = 0 ; i<27 ; i++)
    {
        mem[i]=data.val[i];
        mem[i+27]=mantiss.val[i];
    }
}

void Tfloat::operator =(double num)//3.14 14.112 какого ХРЕНА, ФЛОАТ?!?!??!?!??!
{
    //cout<<"TRANSFERING "<<num<<endl;
    int mant=0;
    int tempI;
    float tempD=num;
    for(int i = 0 ; ;i++)
    {
        tempI = (int)tempD; //кастуем к инту , при этом дробная часть отбрасывается
              // cout<<"tempI "<<tempI<<endl;
               //cout<<"tempD "<<(double)tempD<<endl;
               //cout<<tempD-tempI<<endl;
        if(tempD == tempI)//если нет разницы
        {
          data=Tryte(translate(tempI));
          //data.show();
          mant = i;
          break;
        }
        else
        {
            tempD*=10;
        }
    }

    mantiss=translate(mant);
}

void Tfloat::operator =(Trint trint)
{
    data = trint.data;
    mantiss = translateInt(0);
}

Tfloat::operator float()
{
    //cout<<"starting to float()"<<endl;
    float ret=translate(data);
    //cout<<ret<<endl;
    //cout<<translateInt(mantiss)<<endl;
    ret= ret/pow(10,translateInt(mantiss));
    //cout<<ret<<endl;
    //cout<<"ending to float()"<<endl;
    return ret;
}





Command::Command(string Command, string Where, string What)
{
    /*
     read = 1
     write=2
     mov=3
     add=4
     sub=5
     div=6
     div%=7
     mul=8
     Type=9+

     command mov ax 5
     command mov bx ax
     */
    if(Command == "read")
    {
        command=1;
    }
    else if(Command == "write")
    {
        command=2;
    }
    else if(Command == "mov")
    {
        command=3;
    }
    else if(Command == "add")
    {
        command=4;
    }
    else if(Command == "sub")
    {
        command=5;
    }
    else if(Command == "div")
    {
        command=6;
    }
    else if(Command == "div%")
    {
        command=7;
    }
    else if(Command == "mul")
    {
        command=8;
    }
    else if(Command == "trint")
    {
        command=9;
    }
    else if(Command == "tfloat")
    {
        command=10;
    }
    else if(Command == "tchar")
    {
        command=11;
    }
    else
    {
        cout<<"Unknown command "<<endl;
        command=0;
    }

}

void Command::write(Trit *mem)
{

}

Command::~Command()
{

}





