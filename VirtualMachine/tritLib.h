#ifndef TRIT_H
#define TRIT_H
#include <iostream>
#include <vector>
#include <math.h>
using namespace std;
class Trit;
class tritCode;
class Tryte;
class Trint;
class Tchar;
class Tfloat;
////////////////////////////////////
int translate (tritCode code);
tritCode translate(int number);
tritCode translateInt(int number);
int translateInt(tritCode code);
const Trint operator+(const Trint& left, const Trint& right);
const Trint operator-(const Trint& left, const Trint& right);
const Tfloat operator+(const Tfloat& left,const Tfloat& right);
const Tfloat operator-(const Tfloat& left,const Tfloat& right);
const tritCode operator*( tritCode& left ,  tritCode& right);
const Trint operator*(const Trint& left, const Trint& right);
const Tfloat operator*(const Tfloat& left,const Tfloat& right);
const tritCode operator/( tritCode& left ,  tritCode& right);
const Trint operator/(const Trint& left, const Trint& right);
const Tfloat operator/(const Tfloat& left,const Tfloat& right);
const tritCode operator%( tritCode& left ,  tritCode& right);
const Trint operator%(const Trint& left, const Trint& right);
const Tfloat operator%(const Tfloat& left,const Tfloat& right);
ostream& operator << (ostream &s , Trint output);
ostream& operator << (ostream &s , Tchar output);
ostream& operator << (ostream &s , Tfloat output);

////////////////////////////////////////////////////////////
class tritCode
{
public:
    vector<Trit> code;
    tritCode();

   // tritCode(Trit trits[]);
    tritCode(Trit* trit , int size);
    tritCode(Tryte dat);
    tritCode(int size);
    void pushBack(Trit trit);
    void pushForward(Trit trit);
    bool isEmpty();
    void clear();
    void show();
    void update();
    int size();
    void operator =(tritCode a);
};


class Trit
{
    enum value{
        False=-1,
        Idk=0,
        True=1
    };

public:
    //Var
    value val;
    //func
    Trit();
    Trit(int a);
    void operator =(int a);

};
/////////////////////////////////////////////////////////////////////
/*
 read(adr)
 write(adr)

 */
class Tryte
{
public:
    int size;
    Trit val[27];

    Tryte(Trit trits[27]);
    Tryte(tritCode code);
    Tryte();
    //Tryte(Trit* adress);

    void operator =( Trit mass[27]); //tic
    void read(Trit* adress);
    void write(Trit* adress);
    void show();

};
//////////////////////////////////////////////////////////////////////
class Tchar//сделать конструктор от tritcode
{
public:
    Tryte data;

    Tchar(tritCode dat);
    Tchar(Tryte dat);
    Tchar();
    explicit operator char();
    //ostream& operator << (ostream &s);
    void operator =(char symbol);
    void operator =(Tchar tchar);

};
/////////////////////////////////////////////////////////////////
class Trint//сделать конструктор от триТкода
{
public:
    Tryte data;

    Trint(Tryte dat);
    Trint(tritCode dat);
    Trint();
    void read(Trit* mem);
    void write(Trit* mem);
   explicit operator int();
   // ostream& operator << (ostream &s);
    void operator =(int num);
    void operator =(Trint trint);

    //const Trint operator +(const Trint &right);

};
/////////////////////////////////////////////////////////////
class Tfloat
{
public:
    Tryte data;
    Tryte mantiss;

    Tfloat();
    Tfloat(Tryte d , Tryte m);//мантисса и число могут храниться в разных местах?
    void read(Trit* mem);
    void write(Trit* mem);
    void operator =(double num);
    void operator =(Trint trint);
    explicit operator float();
};
///////////////////////////////////////////////////////////////
class Command
{
    /*
     read = 1
     write=2
     mov=3
     add=4
     sub=5
     div=6
     div%=7
     mul=8
     Type=9

     command mov ax 5
     command mov bx ax
     */
public:
    tritCode code;

    Trint command;
    Trint where;
    Trint what;

    Command(string Command , string Where , string What);
    void write(Trit* mem);
    ~Command();
};

#endif // TRIT_H
