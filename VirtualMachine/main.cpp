#include <iostream>
#include <tritLib.h>
#include <math.h>
#include "cpu.h"
using namespace std;

void showSegment(Trit memory[1289], int seg)
{
    int tryteSeg = 27;
    int current=0;
    for(int i = 0 ; i<6 ; i++)
    {
        for(int j = current + seg*tryteSeg ; j<(current+seg*tryteSeg)+27 ; j++)
        {
            cout<<memory[j].val<<" ";
        }
        current+=27;
        cout<<endl;
    }
}



int main()
{
    srand(time(NULL));
    Trit memory[1287];//[59049];
    //memory[26]=0;
    //memory[25]=-1;
    //memory[24]=1;
    memory[27]=1;
    memory[26]=-1;
    memory[25]=1;
    memory[24]=1;
    memory[23]=1;
    memory[22]=1;
    memory[53]=1;
    memory[52]=1;
    memory[51]=1;
    showSegment(memory,0);
    Tryte testTryte;
    testTryte.read(&memory[0]);
    tritCode test(&memory[22],5);
    test.show();
    int kek;
    kek=translate(test);
    cout<<kek<<endl;
    kek=translateInt(test);
    cout<<kek<<endl;

    Tchar CH(test);
    CH.data.show();
    cout<<CH;

    cout<<"tritCode test"<<endl;
    tritCode tritcode(&memory[21],3);
    tritcode.show();
    tritcode.pushBack(Trit(false));
    tritcode.show();
    tritcode.pushForward(Trit(false));
    tritcode.show();
    test=translate(5);
    cout<<"test in main= ";test.show();
    cout<<"TritCode test end"<<endl<<endl;

    cout<<"testing INT transfer"<<endl;
    test = translateInt(-5);
    test.show();
    cout<<translateInt(test)<<endl;

    cout<<endl;

    //testTryte.show();

    Tchar tchar(testTryte);
    cout<<"Tchar: "<<(char)tchar<<endl; //проверки Tchar
    cout<<"couting tchar"<<endl;
    cout<<tchar;//<<endl;
    cout<<"couting tchar end"<<endl;
    tchar='a';
    cout<<tchar;//<<endl;
    Tchar b;
    b=tchar;
    cout<<b<<endl;
    cout<<"Trint-------------------------------------------------------"<<endl;
    Trint trint(testTryte); //проверки Trint
    cout<<(int)trint<<endl;
    cout<<(int)trint<<endl;
    Trint num;
    num=-27;
    cout<<"num:"<<(int)num<<endl;
    trint=num;
    cout<<"trint:"<<(int)trint<<endl;
    num=-5;
    Trint omg;
    num.data.show();
    trint.data.show();
    cout<<endl;
    cout<<"num="<<(int)num<<endl;
    cout<<"trint="<<(int)trint<<endl;
    omg = num + trint;
    cout<<"omg:"<<(int)omg<<endl;
    Tryte pp;
    pp.read(&memory[27]);
    //cout<<Trint(pp)<<endl;

    Trint one;
    one = 41;
    cout<<"!!!!!!!!!!!!"<<endl;
    one.data.show();
    Trint two;
    two= 13;
    two.data.show();
    omg = one + two;
    cout<<(int)omg<<" -plus ok"<<endl;
    omg = one - two;
    cout<<(int)omg<<" -minus ok"<<endl;
    omg= two - one;
    cout<<(int)omg<<" -minus ok"<<endl;
    omg.read(&memory[27]);
    cout<<(int)omg<<" - red from memory"<<endl;
    int a=(int)omg;
    cout<<a<<" - (int) Test"<<endl;
    omg.read(&memory[27]);
    omg.data.show();
    cout<<endl<<"one="<<(int)one<<endl;
    one.write(&memory[81]);
    two.read(&memory[81]);
    cout<<(int)two<<"-two"<<endl;
    showSegment(memory,0);

    Trint p;
    Trint l;//21.5 2.23
    p=51212412;
    l=6;
    cout<<p<<endl;
    cout<<"NEXT LEVEL----------------------------------"<<endl;
    Tfloat fl;
    fl=5.121;
    cout<<"Casting to float:"<<(float)fl<<" Without casting:"<<fl<<endl;

    Tfloat fk;
    Tfloat lk;
    fk=14.14;
    lk=12.224;

    cout<<fk<<" "<<lk<<endl;
   // lk.data.show();
    fl = lk + fk;
    cout<<lk<<"+"<<fk<<"="<<fl<<" -plus ok"<<endl;
    fl = lk - fk;
    cout<<lk<<"-"<<fk<<"="<<fl<<" -minus ok"<<endl;

    tritCode beta(&memory[22],3); //перемножение двух триткодов
    tritCode alpha(&memory[51],3);
    alpha.show();
    beta.show();
    alpha = alpha * beta;
     cout<<"WHATA!)!(#)!@(!@$!@$!@$"<<endl;
    alpha.show();
    Trint ppp(alpha);
    cout<<ppp<<endl;

    one = 4;//перемножение двух интов
    two = 12;
    one = one * two;
    one.data.show();
    cout<<one<<" - Trint *"<<endl;


    fl=12.1;//перемножение двух флоатов
    fk=2.1;
    fl = fl * fk;
    cout<<fl<<endl;

    tritCode gamma(&memory[22],5);//деление двух триткодов без остатка
    tritCode delta(&memory[51],3);
    gamma = gamma/delta;
    cout<<Trint(gamma)<<" tritCode / is ok"<<endl;

    one = -100; //деление и остаток от деления тринтов
    two = -25;
    one = one/two;
    cout<<one<<" trint / is ok"<<endl;
    one = -101;
    two = -25;
    one = one%two;
    cout<<one<<" trint % is ok"<<endl;

    fl=9.2; //деление и остаток от деления флоатов
    fk=0.2;
    fl=fl/fk;
    cout<<fl<<" tfloat / is ok"<<endl;
    fl=-9.2; //деление и остаток от деления флоатов
    fk=0.2;
    fl=fl/fk;
    cout<<fl<<" tfloat / is ok"<<endl;
    fl=1.5; //деление и остаток от деления флоатов
    fk=2;
    fl=fl/fk;
    cout<<fl<<" tfloat / is ok"<<endl;
    fl=-1.5; //деление и остаток от деления флоатов
    fk=2;
    fl=fl/fk;
    cout<<fl<<" tfloat / is ok"<<endl;
    fl=0.2; //деление и остаток от деления флоатов
    fk=0.25;
    fl=fl/fk;
    cout<<fl<<" tfloat / is ok"<<endl;
    fl=-0.2; //деление и остаток от деления флоатов
    fk=0.25;
    fl=fl/fk;
    cout<<fl<<" tfloat / is ok"<<endl;

    tritCode gama(&memory[22],5);//остаток от деления двух триткодов
    tritCode dela(&memory[51],3);
    cout<<Trint(gama)<<" "<<Trint(dela)<<endl;
    gama = gama%dela;
    cout<<Trint(gama)<<" tritCode % is ok"<<endl;



      system("cls");
     // CPU cpu(memory);
      CPU cpu(1287);
      cpu.showMemorySegment(0);

    cout<<"end of prog";

    return 0;
}

