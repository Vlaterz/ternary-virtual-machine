#ifndef CPU_H
#define CPU_H
#include "tritLib.h"
#include <list>
#include <tuple>
#include <vector>
#include <time.h>
#include <stdio.h>
#include <stack>
#include <map>
using namespace std;

class CPU
{
    /*
     команды
     show ... вывести сегмент
     show ... ... вывести от одной ячейки до другой

     mov (reg) (num,reg) записать в регистр
     add (reg) (reg) //сложить 1 регистр с 2
     mul (reg) (reg) //перемножить 1 рег с 2
     div (reg) (reg) //деление без остатка
     div% (reg) (reg)//остаток от деления

     write (adr;var) (reg,var;num)
     * */
public:
    Trit* memory;
    int size;

    list<tuple<string,string,int>> variables;//таблица переменных

   /*
    Trint ax;
    Trint bx;
    Trint cx;
    Trint max;//мантиссы
    Trint mbx;
    Trint mcx;
    */
    map<string,Trint> registers;

    CPU();
    CPU(int Size);
    CPU(Trit *memo);

    double transferToNum(string a);
    string transferToString(double a);

    void showMemorySegment(int seg);
    void showVariables();
    void showRegisters();
    void run();
    void write(Tryte data , int cell);
    Tryte read(int cell);

    bool isType(string str);
    bool varExists(string name);
    bool isNum(string a);
    bool isCommand(string a);
    bool isInt(double a);

    vector<string> lexer(string str);
    int parcer(vector<string> lexems);
};

#endif // CPU_H
