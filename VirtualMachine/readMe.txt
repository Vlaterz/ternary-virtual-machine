Троичная виртуальная машина
Реализованы типы : trint , tfloat , tchar
ассемблерные команды для регистров и работы с памятью : show , read , write , mov , add , sub , mul , div , div% , swap 
пример кода на ассемблере :
mov ax 5
mov bx 18
add ax bx
(ax = 23)
mov bx 2
div ax bx
(ax=11)
div% ax bx
(ax = 1)